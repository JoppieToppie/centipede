﻿using Centipede.GameObjects;

namespace Centipede.GameStates
{
    class GameOverState : GameObjectList
    {
        public GameOverState()
        {
            Add(new GameOverText());
        }
    }
}
