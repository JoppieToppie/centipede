﻿using Centipede.GameObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Centipede.GameStates
{
    class PlayingState : GameObjectList
    {
        private const int SnakeSize = 10;
        private const int MushroomCount = 20;
        private const int MinMushroomXPos = 20;
        private const int MaxMushroomXPos = 450;
        private const int MinMushroomYPos = 25;
        private const int MaxMushroomYPos = 450;

        private int _scoreCounter;

        private readonly Player _player;
        private readonly GameObjectList _bullets;
        private GameObjectList _snake;
        private GameObjectList _mushrooms;
        private readonly Score _score;
            
        public PlayingState()
        {
            _player = new Player();
            _bullets = new GameObjectList();
            _score = new Score();

            Add(new SpriteGameObject("spr_background"));
            Add(_player);
            Add(_bullets);

            SpawnSnake();
            SpawnMushrooms();

            Add(_score);
        }

        private void SpawnSnake()
        {
            _snake = new GameObjectList();

            for (var i = 0; i < SnakeSize; i++)
            {
                _snake.Add(new SnakeSegment(new Vector2(i * 32, 0)));
            }

            Add(_snake);
        }

        private void SpawnMushrooms()
        {
            _mushrooms = new GameObjectList();

            for (var i = 0; i < MushroomCount; i++)
            {
                var mushroom = new Mushroom(new Vector2(GameEnvironment.Random.Next(MinMushroomXPos, MaxMushroomXPos), GameEnvironment.Random.Next(MinMushroomYPos, MaxMushroomYPos)));

                _mushrooms.Add(mushroom);
            }

            Add(_mushrooms);
        }

        public override void HandleInput(InputHelper inputHelper)
        {
            base.HandleInput(inputHelper);

            if (inputHelper.KeyPressed(Keys.Space))
            {
                _bullets.Add(new Bullet(new Vector2(_player.Position.X, _player.Position.Y - 20f)));
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            HandleSnakeToMushroomCollision();
            HandleBulletToMushroomCollision();
            HandleBulletToSnakeSegmentCollision();
            HandleSnakeSegmentToPlayerCollision();

            _score.Text = _scoreCounter.ToString();
        }

        private void HandleSnakeToMushroomCollision()
        {
            foreach (var snakeGameObject in _snake.Children)
            {
                var snakeSegment = (SnakeSegment) snakeGameObject;

                foreach (var mushroomGameObject in _mushrooms.Children)
                {
                    var mushrooms = (Mushroom) mushroomGameObject;

                    if(snakeSegment.CollidesWith(mushrooms))
                    {
                        snakeSegment.Bounce();
                    }
                }
            }
        }

        private void HandleBulletToMushroomCollision()
        {
            foreach (var bulletGameObject in _bullets.Children)
            {
                var bullet = (Bullet) bulletGameObject;

                foreach (var mushroomGameObject in _mushrooms.Children)
                {
                    var mushrooms = (Mushroom) mushroomGameObject;

                    if (bullet.CollidesWith(mushrooms))
                    {
                        bullet.Visible = false;
                        mushrooms.Visible = false;

                        _scoreCounter++;
                    }
                }
            }
        }

        private void HandleBulletToSnakeSegmentCollision()
        {
            foreach (var bulletGameObject in _bullets.Children)
            {
                var bullet = (Bullet) bulletGameObject;

                foreach (var snakeGameObject in _snake.Children)
                {
                    var snakeSegment = (SnakeSegment) snakeGameObject;

                    if (bullet.CollidesWith(snakeSegment))
                    {
                        bullet.Visible = false;
                        snakeSegment.Visible = false;

                        _mushrooms.Add(new Mushroom(snakeSegment.Position));
                        _scoreCounter += 10;
                    }
                }
            }
        }

        private void HandleSnakeSegmentToPlayerCollision()
        {
            foreach (var snakeGameObject in _snake.Children)
            {
                var snakeSegment = (SnakeSegment) snakeGameObject;

                if (snakeSegment.CollidesWith(_player))
                {
                    GameEnvironment.GameStateManager.SwitchTo("GameOver");
                }
            }
        }
    }
}
