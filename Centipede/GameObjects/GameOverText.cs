﻿namespace Centipede.GameObjects
{
    class GameOverText : TextGameObject
    {
        public GameOverText() : base("GameFont")
        {
            position.X = GameEnvironment.Screen.X / 2f - 60f;
            position.Y = GameEnvironment.Screen.Y / 2f;

            text = "Game Over!";
        }
    }
}
