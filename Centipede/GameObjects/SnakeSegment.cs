﻿using Microsoft.Xna.Framework;

namespace Centipede.GameObjects
{
    class SnakeSegment : SpriteGameObject
    {
        public SnakeSegment(Vector2 spawnPosition) : base("spr_snakebody")
        {
            position = spawnPosition;
            velocity.X = 200f;
        }

        public void Bounce()
        {
            velocity.X = -velocity.X;
            position.Y += 32f;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (position.X < 0 || position.X > GameEnvironment.Screen.X - Width)
            {
                Bounce();
            }
        }
    }
}
