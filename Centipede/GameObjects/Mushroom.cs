﻿using Microsoft.Xna.Framework;

namespace Centipede.GameObjects
{
    class Mushroom : SpriteGameObject
    {
        public Mushroom(Vector2 spawnPosition) : base("spr_mushroom")
        {
            position = spawnPosition;
        }
    }
}
