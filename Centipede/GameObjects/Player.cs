﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Centipede.GameObjects
{
    class Player : SpriteGameObject
    {
        public Player() : base("spr_player")
        {
            Mouse.SetPosition(235, 500);
            Origin = new Vector2(Width / 2f, Height / 2f);
        }

        public override void HandleInput(InputHelper inputHelper)
        {
            base.HandleInput(inputHelper);

            position = inputHelper.MousePosition;
        }
    }
}
