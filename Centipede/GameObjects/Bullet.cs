﻿using Microsoft.Xna.Framework;

namespace Centipede.GameObjects
{
    class Bullet : SpriteGameObject
    {
        public Bullet(Vector2 spawnPosition) : base("spr_bullet")
        {
            position = spawnPosition;
            velocity.Y = -200f;
            origin = new Vector2(Width / 2f, Height / 2f);
        }
    }
}
